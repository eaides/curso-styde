<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WelcomeTest extends TestCase
{
    /** @test */
    function it_loads_the_salutation_without_nickname()
    {
        $this->get('saludo/ernesto')
            ->assertStatus(200)
            ->assertSee('Bienvenido Ernesto');
    }

    /** @test */
    function it_loads_the_salutation_with_nickname()
    {
        $this->get('saludo/ernesto/laravel')
            ->assertStatus(200)
            ->assertSee('Bienvenido Ernesto, tu apodo es laravel');
    }
}
