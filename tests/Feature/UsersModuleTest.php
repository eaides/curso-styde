<?php

namespace Tests\Feature;

use App\User;
use App\UserProfile;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersModuleTest extends TestCase
{
    use RefreshDatabase;

    /**
     * -- for avoid that method name start with 'test...'
     * ensure to add the annotation \@test\ in the PHPDoc
     * -- the public definition for the method is optional (by default methods in php are public)
     * @test
     */
    function it_show_the_users_list()
    {
        factory(User::class, 1)
            ->create([
                'name' => 'Ernesto',
                ]
            )->each(function($u) {
                $u->profile()->save(factory(UserProfile::class)->make());
            }
        );

        factory(User::class, 1)
            ->create([
                'name' => 'Patricia',
                ]
            )->each(function($u) {
                $u->profile()->save(factory(UserProfile::class)->make());
            }
        );

        $this->get(route('users.index'))
            ->assertStatus(200)
            ->assertSee('Lista de usuarios')
            ->assertSee('Ernesto')
            ->assertSee('Patricia');
    }

    /** @test */
    function it_shows_a_default_message_if_there_are_not_users()
    {
        $this->get(route('users.index'))
            ->assertStatus(200)
            ->assertSee('Lista de usuarios')
            ->assertSee('No hay usuarios registrados');
    }

    /** @test */
    function it_display_the_users_details()
    {
        // $this->withoutExceptionHandling();
        $user = factory(User::class, 1)->create([
            'name' => 'Ernesto',
            'email' => 'ernesto@hotmail.com'
        ])->each(function($u) {
            $u->profile()->save(factory(UserProfile::class)->make(
                [
                    'bio' => 'mi bio',
                    'twitter' => '@mi_twitter'
                ]
            ));
        })->first();

        $this->get(route('users.show',$user))
            ->assertStatus(200)
            ->assertSee('Detalle del usuario: ' . $user->id)
            ->assertSee('Ernesto')
            ->assertSee('ernesto@hotmail.com')
            ->assertSee('mi bio')
            ->assertSee('@mi_twitter');
    }

    /** @test */
    function it_fails_to_display_the_users_details_for_a_non_existing_id()
    {
        $this->get(route('users.show',101))
            ->assertStatus(404);
    }

    /** @test */
    function it_loads_the_user_create_page()
    {
        // $this->withoutExceptionHandling();
        $this->get(route('users.create'))
            ->assertStatus(200)
            ->assertSee('Crear nuevo usuario');
    }

    /** @test **/
     function it_creates_a_new_user()
     {
        // $this->withoutExceptionHandling();

        $this->post(route('users.store'),
            $this->getDataTest([
                'bio' => 'programador php y laravel',
                'twitter' => '@enaides',
            ])
        )->assertRedirect(route('users.index'));
            // ->assertSee('Procesando Informacion')
            // ->assertStatus(200);

        $this->assertDatabaseHas('users',[
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
        ]);

         $this->assertDatabaseHas('user_profiles',[
             'bio' => 'programador php y laravel',
             'twitter' => '@enaides',
             'user_id' => User::first()->id,
         ]);

         $this->assertCredentials([
            'email' => 'eaides@hotmail.com',
            'password' => '123456',
        ]);
     }

    /** @test **/
    function field_bio_has_rules_create_a_user()
    {
        // $this->withoutExceptionHandling();
        $this->from(route('users.create'))
            ->post(route('users.store'),
                $this->getDataTest([
                    'bio' => '',
                    'twitter' => '',
                ])
            )
            ->assertRedirect(route('users.create'))
            ->assertSessionHasErrors(['bio']);

        $this->assertEquals(0, User::count());

        $this->from(route('users.create'))
            ->post(route('users.store'),
                $this->getDataTest([
                    'bio' => 'corto',
                    'twitter' => null,
                ])
            )
            ->assertRedirect(route('users.create'))
            ->assertSessionHasErrors(['bio']);

        $this->assertEquals(0, User::count());

        $this->from(route('users.create'))
            ->post(route('users.store'),
                $this->getDataTest([
                    'bio' => '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890xxxx',
                    'twitter' => null,
                ])
            )
            ->assertRedirect(route('users.create'))
            ->assertSessionHasErrors(['bio']);

        $this->assertEquals(0, User::count());

        $this->from(route('users.create'))
            ->post(route('users.store'),
                $this->getDataTest([
                    'bio' => 'abcdef',
                   'twitter' => null,
                ])
            )
            ->assertSessionHasNoErrors();

        $this->from(route('users.create'))
            ->post(route('users.store'),
                $this->getDataTest([
                    'email' => 'other@hotmail.com',
                    'bio' => '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890',
                    'twitter' => null,
                ])
            )
            ->assertSessionHasNoErrors();
    }

    /** @test **/
    function field_name_is_mandatory_when_create_a_user_without_do_the_redirect()
    {
        // if we enable the following without excetion, laravel will not redirect
        // in case of validation error,, becouse the redirection is aprt of the exception handling
        //$this->withoutExceptionHandling();

        $this->from(route('users.create'))
            ->post(route('users.store'),
                $this->getDataTest([
                    'name' => null,
                ])
            )
            ->assertRedirect(route('users.create'))
            ->assertSessionHasErrors(['name']);
            // can not check this because the test does not do the redirect
            // but returns a view with the message "redirecting to..."
            // ->assertSee('Corrija los siguientes errores');

        $this->assertEquals(0, User::count());
    }

    /** @test **/
    function field_name_is_mandatory_when_create_a_user_doing_the_redirect()
    {
        // if we enable the following without excetion, laravel will not redirect
        // in case of validation error,, becouse the redirection is aprt of the exception handling
        //$this->withoutExceptionHandling();

        $this->followingRedirects();

        $this->from(route('users.create'))
            ->post(route('users.store'),
                $this->getDataTest([
                    'name' => null,
                ])
            )
            ->assertSee('Corrija los siguientes errores');

            // here can not check the assertSessionHasErrors because the view was already
            // rendered and the errors are "flash messages" and therefore there were already gone
            // ->assertSessionHasErrors(['name']);

            // here can not check the assertRedirect because the redirect
            // was already done, and therefore receive a status 200 instead code for redirects
            // ->assertRedirect(route('users.create'));

        $this->assertEquals(0, User::count());
    }

    /** @test **/
    function field_email_is_mandatory_when_create_a_user()
    {
        $this->from(route('users.create'))
            ->post(route('users.store'),
                $this->getDataTest([
                    'email' => '',
                ])
            )
            ->assertRedirect(route('users.create'))
            ->assertSessionHasErrors(['email']);

        $this->assertEquals(0, User::count());
    }

    /** @test **/
    function field_email_is_an_valid_email_when_create_a_user()
    {
        $this->from(route('users.create'))
            ->post(route('users.store'),
                $this->getDataTest([
                    'email' => 'not-a-valid-email',
                ])
            )
            ->assertRedirect(route('users.create'))
            ->assertSessionHasErrors(['email']);

        $this->assertEquals(0, User::count());
    }

    /** @test **/
    function field_email_is_unique_when_create_a_user()
    {
        factory(User::class, 1)->create([
            'email' => 'ernesto@hotmail.com'
        ])->each(function($u) {
            $u->profile()->save(factory(UserProfile::class)->make());
        });

        $this->from(route('users.create'))
            ->post(route('users.store'),
                $this->getDataTest([
                    'email' => 'ernesto@hotmail.com',
                ])
            )
            ->assertRedirect(route('users.create'))
            ->assertSessionHasErrors(['email']);

        $this->assertEquals(1, User::count());
    }

    /** @test **/
    function field_password_is_mandatory_when_create_a_user()
    {
        $this->from(route('users.create'))
            ->post(route('users.store'),
                $this->getDataTest([
                    'password' => null,
                ])
            )
            ->assertRedirect(route('users.create'))
            ->assertSessionHasErrors(['password']);

        $this->assertEquals(0, User::count());
    }

    /** @test **/
    function field_password_is_min_6_chars_when_create_a_user()
    {
        $this->from(route('users.create'))
            ->post(route('users.store'),
                $this->getDataTest([
                    'password' => '123',
                ])
            )
            ->assertRedirect(route('users.create'))
            ->assertSessionHasErrors(['password']);

        $this->assertEquals(0, User::count());
    }

    /** @test */
    function it_loads_the_edit_user_page()
    {
        $user = factory(User::class, 1)
            ->create(
                [
                    'name' => 'Ernesto',
                    'email' => 'ernesto@hotmail.com'
                ]
            )->each(function($u) {
                $u->profile()->save(factory(UserProfile::class)->make());
            }
        )->first();

        // $this->withoutExceptionHandling();
        $this->get(route('users.edit',$user))
            ->assertStatus(200)
            ->assertViewIs('users.edit')
            ->assertSee('Editar usuario')
            ->assertViewHas('user', function($viewUser) use ($user) {
                return $viewUser->id === $user->id;
            });
    }

    /** @test **/
    function it_update_a_given_user()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class, 1)
            ->create()
            ->each(function($u) {
                $u->profile()->save(factory(UserProfile::class)->make());
            }
        )->first();
        $this->put(route('users.update',$user),[
            'name' => 'Ernesto',
            'email' => 'eaides@hotmail.com',
            'password' => '123456',
            'bio' => 'this is the bio',
        ])->assertRedirect(route('users.show', ['id' => $user->id]));

        $this->assertDatabaseHas('users',[
            'name' => 'Ernesto',
            'email' => 'eaides@hotmail.com',
        ]);

        $this->assertDatabaseHas('user_profiles',[
            'id' => $user->id,
            'bio' => 'this is the bio',
        ]);

        $this->assertCredentials([
            'name' => 'Ernesto',
            'email' => 'eaides@hotmail.com',
            'password' => '123456',
        ]);
    }

    /** @test **/
    function field_name_is_mandatory_when_update_the_user()
    {
        // $this->withoutExceptionHandling();

        $user = factory(User::class, 1)->create()
            ->each(function($u) {
                $u->profile()->save(factory(UserProfile::class)->make());
        })->first();

        $this->from(route('users.edit',['user'=>$user]))
            ->put(route('users.update',$user),[
            'name' => '',
            'email' => 'eaides@hotmail.com',
            'password' => '123456',
        ])->assertRedirect(route('users.edit',['user'=>$user]))
            ->assertSessionHasErrors(['name']);

        $this->assertDatabaseMissing('users',['email'=>'eaides@hotmail.com']);
    }

    /** @test **/
    function field_email_is_mandatory_when_update_the_user()
    {
        // $this->withoutExceptionHandling();

        $user = factory(User::class, 1)->create()->each(function($u) {
            $u->profile()->save(factory(UserProfile::class)->make());
        })->first();

        $this->from(route('users.edit',['user'=>$user]))
            ->put(route('users.update',$user),[
                'name' => 'Ernesto Aides',
                'email' => '',
                'password' => '123456',
            ])->assertRedirect(route('users.edit',['user'=>$user]))
            ->assertSessionHasErrors(['email']);

        $this->assertDatabaseMissing('users',['name'=>'Ernesto Aides']);
    }

    /** @test **/
    function field_email_is_an_email_when_update_the_user()
    {
        // $this->withoutExceptionHandling();

        $user = factory(User::class, 1)->create()->each(function($u) {
            $u->profile()->save(factory(UserProfile::class)->make());
        })->first();

        $this->from(route('users.edit',['user'=>$user]))
            ->put(route('users.update',$user),[
                'name' => 'Ernesto Aides',
                'email' => 'correo-no-valido',
                'password' => '123456',
            ])->assertRedirect(route('users.edit',['user'=>$user]))
            ->assertSessionHasErrors(['email']);

        $this->assertDatabaseMissing('users',['name'=>'Ernesto Aides']);
    }

    /** @test **/
    function field_email_is_unique_when_update_the_user()
    {
        // $this->withoutExceptionHandling();

        $user = factory(User::class, 1)->create([
            'email' => 'eaides@hotmail.com'
        ])->each(function($u) {
            $u->profile()->save(factory(UserProfile::class)->make());
        })->first();

        $other_user = factory(User::class, 1)->create([
            'email' => 'other@example.com'
        ])->each(function($u) {
            $u->profile()->save(factory(UserProfile::class)->make());
        })->first();

        $this->from(route('users.edit',['user'=>$user]))
            ->put(route('users.update',$user),[
                'name' => 'Ernesto Aides',
                'email' => 'other@example.com',
                'password' => '123456',
            ])->assertRedirect(route('users.edit',['user'=>$user]))
            ->assertSessionHasErrors(['email']);

        $this->assertDatabaseMissing('users',['name'=>'Ernesto Aides']);
    }

    /** @test **/
    function field_email_is_unique_when_update_the_user_but_accept_mine()
    {
        // $this->withoutExceptionHandling();

        $user = factory(User::class, 1)->create([
            'email' => 'eaides@hotmail.com'
        ])->each(function($u) {
            $u->profile()->save(factory(UserProfile::class)->make());
        })->first();

        $other_user = factory(User::class, 1)->create([
            'email' => 'other@example.com'
        ])->each(function($u) {
            $u->profile()->save(factory(UserProfile::class)->make());
        })->first();

        $this->from(route('users.edit',['user'=>$user]))
            ->put(route('users.update',$user),[
                'name' => 'Ernesto Aides',
                'email' => 'eaides@hotmail.com',
                'password' => '123456',
                'bio' => 'a bio paragraph',
            ])->assertRedirect(route('users.show', ['id' => $user->id]));

        $this->assertDatabaseHas('users',[
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
        ]);

        $this->assertCredentials([
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
            'password' => '123456',
        ]);
    }

    /** @test **/
    function field_password_is_optional_when_update_the_user()
    {
        $oldPassword = 'CLAVE_ANTERIOR';
        $user = factory(User::class, 1)->create([
            'password' => bcrypt($oldPassword)
        ])->each(function($u) {
            $u->profile()->save(factory(UserProfile::class)->make());
        })->first();

        $this->from(route('users.edit',['user'=>$user]))
            ->put(route('users.update',$user),
                $this->getDataTest([
                    'password' => null
                ])
            )->assertRedirect(route('users.show', ['id' => $user->id]));

        $this->assertDatabaseHas('users',[
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
        ]);

        $this->assertCredentials([
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
            'password' => $oldPassword,
        ]);
    }

    /** @test **/
    function field_password_is_min_6_chars_when_update_the_user_with_password()
    {
        $oldPassword = 'CLAVE_ANTERIOR';
        $user = factory(User::class, 1)->create([
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
            'password' => bcrypt($oldPassword),
        ])->each(function($u) {
            $u->profile()->save(factory(UserProfile::class)->make());
        })->first();

        $this->from(route('users.edit',['user'=>$user]))
            ->put(route('users.update',$user),[
                'name' => 'Ernesto Aides',
                'email' => 'eaides@hotmail.com',
                'password' => '123',
                'bio' => 'a bio paragraph',
            ])->assertRedirect(route('users.edit',['user'=>$user]))
            ->assertSessionHasErrors(['password']);

        $this->assertCredentials([
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
            'password' => $oldPassword,
        ]);
    }

    /** @test **/
    function field_password_can_be_updated_the_user_with_password()
    {
        $oldPassword = 'CLAVE_ANTERIOR';
        $user = factory(User::class, 1)->create([
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
            'password' => bcrypt($oldPassword)
        ])->each(function($u) {
            $u->profile()->save(factory(UserProfile::class)->make());
        })->first();

        $this->from(route('users.edit',['user'=>$user]))
            ->put(route('users.update',$user),[
                'name' => 'Ernesto Aides',
                'email' => 'eaides@hotmail.com',
                'password' => 'eaides@hotmail.com',
                'bio' => 'a bio paragraph',
            ])->assertRedirect(route('users.show', ['id' => $user->id]));

        $this->assertCredentials([
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
            'password' => 'eaides@hotmail.com',
        ]);
    }

    /** @test **/
    function field_bio_has_rules_when_updates_a_user()
    {
        $user = factory(User::class, 1)->create([
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
            'password' => bcrypt('123456')
        ])->each(function($u) {
            $u->profile()->save(factory(UserProfile::class)->make());
        })->first();
        $bio = $user->profile->bio;

        // $this->withoutExceptionHandling();
        $this->from(route('users.edit',['user'=>$user]))
            ->put(route('users.update',$user),[
                'name' => 'Ernesto Changed',
                'email' => 'eaides2@hotmail.com',
                'password' => '123456',
                'bio' => '',
                'twitter' => ''
        ])->assertRedirect(route('users.edit',['user'=>$user]))
            ->assertSessionHasErrors(['bio']);

        $this->assertDatabaseHas('users',[
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
        ]);

        $this->assertDatabaseHas('user_profiles',[
            'id' => $user->id,
            'bio' => $bio,
        ]);

        $this->assertDatabaseMissing('users',[
            'name' => 'Ernesto Changed',
            'email' => 'eaides2@hotmail.com',
        ]);

        $this->from(route('users.edit',['user'=>$user]))
                ->put(route('users.update',$user),[
            'name' => 'Ernesto',
            'email' => 'eaides@hotmail.com',
            'password' => '123456',
            'bio' => '--',
            'twitter' => '',
        ])->assertRedirect(route('users.edit',['user'=>$user]))
            ->assertSessionHasErrors(['bio']);

        $this->assertDatabaseHas('users',[
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
        ]);

        $this->assertDatabaseHas('user_profiles',[
            'id' => $user->id,
            'bio' => $bio,
        ]);

        $this->assertDatabaseMissing('users',[
            'name' => 'Ernesto Changed',
            'email' => 'eaides2@hotmail.com',
        ]);

        $this->from(route('users.edit',['user'=>$user]))
                ->put(route('users.update',$user),[
            'name' => 'Ernesto',
            'email' => 'eaides@hotmail.com',
            'password' => '123456',
            'bio' => '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890xxxx',
            'twitter' => '',
        ])->assertRedirect(route('users.edit',['user'=>$user]))
            ->assertSessionHasErrors(['bio']);

        $this->assertDatabaseHas('users',[
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
        ]);

        $this->assertDatabaseHas('user_profiles',[
            'id' => $user->id,
            'bio' => $bio,
        ]);

        $this->assertDatabaseMissing('users',[
            'name' => 'Ernesto Changed',
            'email' => 'eaides2@hotmail.com',
        ]);

        $this->from(route('users.edit',['user'=>$user]))
            ->put(route('users.update',$user),[
            'name' => 'Ernesto1',
            'email' => 'eaides1@hotmail.com',
            'password' => '123456',
            'bio' => '123456',
            'twitter' => '',
        ])->assertSessionHasNoErrors();

        $this->assertDatabaseHas('users',[
            'name' => 'Ernesto1',
            'email' => 'eaides1@hotmail.com',
        ]);

        $this->assertDatabaseHas('user_profiles',[
            'id' => $user->id,
            'bio' => '123456',
        ]);

        $this->from(route('users.edit',['user'=>$user]))
            ->put(route('users.update',$user),[
            'name' => 'Ernesto2',
            'email' => 'eaides2@hotmail.com',
            'password' => '123456',
            'bio' => '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890',
            'twitter' => '',
        ])->assertSessionHasNoErrors();

        $this->assertDatabaseHas('users',[
            'name' => 'Ernesto2',
            'email' => 'eaides2@hotmail.com',
        ]);

        $this->assertDatabaseHas('user_profiles',[
            'id' => $user->id,
            'bio' => '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890',
        ]);

    }

    /** @test **/
    function it_deletes_a_user()
    {
        // $this->withoutExceptionHandling();

        $user = factory(User::class,1)->create()->each(function($u) {
            $u->profile()->save(factory(UserProfile::class)->make());
        })->first();
        $this->assertEquals(1, User::count());
        $this->assertEquals(1, UserProfile::count());

        $this->delete(route('users.destroy', ['user'=>$user]))
            ->assertRedirect(route('users.index'));

        $this->assertEquals(0, User::count());
        $this->assertEquals(0, UserProfile::count());
    }

    /**
     * @return array
     */
    protected function getDataTest(array $custom = []): array
    {
        return array_filter(array_merge([
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
            'password' => '123456',
            'bio' => 'a bio paragraph',
            'twitter' => '@enaides',
        ], $custom));
    }
}
