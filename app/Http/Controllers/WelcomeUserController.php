<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeUserController extends Controller
{
    /**
     * @brief:
     *   if we have a class with juts one public method that represnt an action
     *     can call that method as __invoke
     *     and the call the object like a function
     *   In the routes we will have just the controller name without the function name:
     *      WelcomeUserController
     *    instead
     *      WelcomeUserController@index)
     *
     * @param $name
     * @param null $nickName
     * @return string
     */
    public function __invoke($name, $nickName=null)
    {
        $name = ucfirst($name);
        if ($nickName) return "Bienvenido {$name}, tu apodo es {$nickName}";
        return "Bienvenido {$name}";
    }
}
