<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        $title = 'Lista de usuarios';

        // call a view passing data whith an associative array
        //        return view('users', [
        //            'users' => $users,
        //            'title' => 'Lista de usuarios'
        //        ]);

        // call a view passing data with the method ->with()
        //        return view('users')
        //            ->with('users', $users)
        //            ->with('title', $title);

        // call a view passing data with php function: compact
        // dd(compact('users', 'title'));   // dd is a laravel helper = to var_dump(...); die();
        return view('users.index', compact('users', 'title'));
    }

    public function show(User $user)
    {
        // $user = User::findOrFail($id);
        $title = 'Detalle del usuario';
        return view('users.show', compact('user', 'title'));
        // return "Detalle del usuario: {$id}";
    }

    public function create()
    {
        $title = 'Crear nuevo usuario';
        return view('users.new', compact('title'));
        // return "Crear nuevo usuario";
    }

    public function store(CreateUserRequest $request)
    {
        $request->createUser();
        // return "Procesando Informacion";
        return redirect()->route('users.index');
    }

    public function edit(User $user)
    {
        $title = 'Editar usuario';
        return view('users.edit', compact('title','user'));
    }

    public function update(User $user, Request $request)
    {
        $rules = [
            'name' => 'required',
            'bio' => 'required|min:6|max:100',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id)
            ],
            'password' => ['nullable','min:6'],
            'twitter' => 'nullable',
        ];
//        if ($request['password'] != null) {
//            $rules['password'] = 'required|min:6';
//        }
//        if ($request['twitter'] != null) {
//            $rules['twitter'] = '';
//        }
        $data = $request->validate($rules);
        $user->name = $data['name'];
        $user->email = $data['email'];
        if ($request['password'] != null) {
            $user->password = bcrypt($data['password']);
        }
        $dataProfiles = [
            'bio' => $data['bio'],
            'twitter' => $data['twitter'] ?? null,
        ];
//        if ($request['twitter'] != null) {
//            $dataProfiles['twitter'] = $data['twitter'];
//        }
        DB::transaction(function() use ($user, $dataProfiles)
        {
            $user->save();
            $user->profile()->update($dataProfiles);
        });

        return redirect()->route('users.show', ['user' => $user]);
    }

    public function destroy(User $user)
    {
        //dd($user);
        DB::transaction(function() use ($user)
        {
            $user->profile()->delete();
            $user->delete();
        });
        return redirect()->route('users.index');
    }
}
