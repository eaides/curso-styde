<?php

Route::get('/', function () {
    return 'home';
});

Route::get('/usuarios', 'UserController@index')
    ->name('users.index');

Route::get('/usuarios/{user}', 'UserController@show')
    ->where('user', '[0-9]+')
    ->name('users.show');

Route::get('/usuarios/nuevo', 'UserController@create')
    ->name('users.create');
Route::post('/usuarios', 'UserController@store')
    ->name('users.store');

Route::get('/usuarios/{user}/editar', 'UserController@edit')
    ->where('user', '[0-9]+')
    ->name('users.edit');
Route::put('/usuarios/{user}', 'UserController@update')
    ->name('users.update');

Route::delete('/usuarios/{user}', 'UserController@destroy')
    ->where('user', '[0-9]+')
    ->name('users.destroy');




Route::get('/saludo/{name}/{nickname?}', 'WelcomeUserController');
