<?php

use App\UserProfile;
use Faker\Generator as Faker;

$factory->define(UserProfile::class, function (Faker $faker) {
    return [
        'bio' => substr($faker->sentence(),0,100),
        'twitter' => '@' . $faker->word,
        'user_id' => 0,
    ];
});
