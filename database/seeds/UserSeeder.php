<?php

use App\Profession;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
// using SQL sentences and obtain an array of results
//        $professions = DB::select('SELECT id FROM professions WHERE title = ?', ['Back-end developer']);
//        $professions_id = $professions[0]->id;

// using query constructor: select and take and get to obtain an collection of objects (limit 1)
//        $professions = DB::table('professions')->select('id')->take(1)->get();
//        $professions_id = $professions->first()->id;

// using query constructor:  select and first to obtain an object for the first occurrence
//        $profession = DB::table('professions')->select('id')->first();
//        $profession_id = $profession->id;

// using query constructor:  value for take a value (instead of object) for the first occurrence
//        $profession_id = DB::table('professions')->value('id');

// using query constructor:  where and value for take a value (instead of object) for the first occurrence
//        $profession_id = DB::table('professions')
//            ->where('title', 'Back-end developer')
//            ->value('id');

// using query constructor: use magic methods for where
//        $profession_id = DB::table('professions')
//            ->whereTitle('Back-end developer')
//            ->value('id');

// using query constructor
//        DB::table('users')->insert([
//            'name' => 'Ernesto Aides',
//            'email' => 'eaides@hotmail.com',
//            'password' => bcrypt('ena2101'),
//            'profession_id' => $profession_id,
//        ]);

        $profession_id = Profession::whereTitle('Back-end developer')
            ->value('id');

        User::create([
            'name' => 'Ernesto Aides',
            'email' => 'eaides@hotmail.com',
            'password' => bcrypt('ena2101'),
            'profession_id' => $profession_id,
        ]);

        factory(User::class)
            ->create([
                'profession_id' => $profession_id,
            ])->each(function($u) {
                $u->profile()->save(factory(App\UserProfile::class)->make());
            });

        factory(User::class, 48)
            ->create()
            ->each(function($u) {
                $u->profile()->save(factory(App\UserProfile::class)->make());
        });
    }
}
