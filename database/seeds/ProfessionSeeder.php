<?php

use App\Profession;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

// using SQL sentences
//        DB::insert('INSERT INTO professions (title) VALUES (:title)',[
//            'title' =>  'Back-end developer',
//        ]);

// using query constructor
//        DB::table('professions')->insert([
//            'title' => 'Front-end developer',
//        ]);
//
//        DB::table('professions')->insert([
//            'title' => 'Back-end developer',
//        ]);

        // using Eloquent Models
        Profession::create([
            'title' => 'Back-end developer',
        ]);

        Profession::create([
            'title' => 'Front-end developer',
        ]);

        factory(Profession::class)->times(18)->create();
    }
}
