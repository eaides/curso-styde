@extends('layout')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>{{ $title }}</h4>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    Corrija los siguientes errores
                    {{--<ul>--}}
                    {{--@foreach($errors->all() as $error)--}}
                    {{--<li>{{ $error }}</li>--}}
                    {{--@endforeach--}}
                    {{--</ul>--}}
                </div>
            @endif

            <form action="{{ route('users.store') }}" method="POST">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">Nombre:</label>
                    <input class="form-control"
                           aria-describedby="nameHelp"
                           type="text" name="name" placeholder="Juan Perez" id="name"
                           value="{{ old('name') }}"
                    >
                    @if ($errors->has('name'))
                        <small id="nameHelp" class="form-text text-danger">{{ $errors->first('name') }}</small>
                    @endif
                </div>

                <div class="form-group">
                    <label for="email">Correo electr&oacute;nico:</label>
                    <input class="form-control"
                           aria-describedby="emailHelp"
                           type="email" name="email" placeholder="jp&eacute;rez@example.com" id="email"
                           value="{{ old('email') }}"
                    >
                    @if ($errors->has('email'))
                        <small id="emailHelp" class="form-text text-danger">{{ $errors->first('email') }}</small>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password">Clave:</label>
                    <input class="form-control"
                           aria-describedby="passwordlHelp"
                           type="password" name="password" placeholder="minimo 6 caracteres" id="password"
                    >
                    @if ($errors->has('password'))
                        <small id="passwordlHelp" class="form-text text-danger">{{ $errors->first('password') }}</small>
                    @endif
                </div>

                <div class="form-group">
                    <label for="name">Biograf&iacute;a:</label>
                    <input class="form-control"
                           aria-describedby="bioHelp"
                           type="text" name="bio" placeholder="descr&iacute;bete (hasta 100 caracteres)" id="bio"
                           value="{{ old('bio') }}"
                    >
                    @if ($errors->has('bio'))
                        <small id="bioHelp" class="form-text text-danger">{{ $errors->first('bio') }}</small>
                    @endif
                </div>

                <div class="form-group">
                    <label for="twitter">Twitter:</label>
                    <input class="form-control"
                           aria-describedby="twitterHelp"
                           type="text" name="twitter" placeholder="@your_twitter" id="twitter"
                           value="{{ old('twitter') }}"
                    >
                    @if ($errors->has('twitter'))
                        <small id="twitterHelp" class="form-text text-danger">{{ $errors->first('twitter') }}</small>
                    @endif
                </div>

                <button class="btn btn-primary" type="submit">Crear usuario</button>
                <a class="btn btn-link" href="{{ route('users.index') }}"><span class="oi oi-list"> Volver al listado</span></a>    {{--(with route)--}}

            </form>
        </div>
    </div>
@endsection

@section('sidebar')
    @parent
    <h3>Personalizada</h3>
@endsection