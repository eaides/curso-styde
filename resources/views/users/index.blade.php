@extends('layout')

@section('content')
    <div class="d-flex justify-content-between align-items-end mb-3">
        <h1 class="pb-1">{{ $title }}</h1>
        <p>
            <a class="btn btn-primary" href="{{ route('users.create') }}">Crear Usuario</a>
        </p>
    </div>
    @if(count($users))
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Usuario</th>
                <th scope="col">Correo electr&oacute;nico</th>
                <th scope="col">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td scope="row">{{ $user->id }}</td>
                    <td>{{ ucfirst($user->name) }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        {{--<a href="{{ url("/usuarios/{$user->id}") }}">Ver Detalles</a>--}}
                        {{--<a href="{{ action("UserController@show", ['id'=>$user->id]) }}">Ver Detalles with action</a>--}}
                        <form action="{{ route('users.destroy', $user) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <a class="btn btn-link" href="{{ route("users.show", $user) }}"><span class="oi oi-eye"></span></a>
                            <a class="btn btn-link" href="{{ route("users.edit", $user) }}"><span class="oi oi-pencil"></span></a>
                            <button type="submit" class="btn btn-outline-danger"><span class="oi oi-trash"></span></button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>No hay usuarios registrados</p>
    @endif
@endsection