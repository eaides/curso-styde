<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Listado de usuarios - Styde.net</title>
</head>
<body>
    <h1><?= e($title); ?></h1>
    <ul>
        <?php foreach($users as $user): ?>
            <li><?= e(ucfirst($user)) ?></li>
        <?php endforeach; ?>
    </ul>
</body>
</html>