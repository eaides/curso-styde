@extends('layout')

@section('title',$title.' - '.$user->id)

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>{{ $title }}: {{ $user->id }}</h4>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td scope="row">Nombre:</td>
                    <td>{{ $user->name }}</td>
                </tr>
                <tr>
                    <td scope="row">Correo electr&oacute;nico:</td>
                    <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <td scope="row">Biograf&iacute;a:</td>
                    <td>{{ $user->profile->bio }}</td>
                </tr>
                <tr>
                    <td scope="row">Twitter:</td>
                    <td>{{ $user->profile->twitter }}</td>
                </tr>
                </tbody>
            </table>
            <p>
                {{--<a href="{{ url('usuarios') }}">Volver al listado</a>--}}
                {{--<br>--}}
                {{--<a href="{{ url()->previous() }}">Volver al listado (with previous)</a>--}}
                {{--<br>--}}
                {{--<a href="{{ action('UserController@index') }}">Volver al listado</a>--}}
                {{--<br>--}}
                <a class="btn btn-link" href="{{ route('users.index') }}"><span class="oi oi-list"> Volver al listado</span></a>    {{--(with route)--}}
                <a class="btn btn-link" href="{{ route('users.edit',['user'=>$user]) }}"><span class="oi oi-pencil"> Editar usuario</span></a>

            </p>
        </div>
    </div>
@endsection