@extends('layout')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>{{ $title }}: {{ $user->id }}</h4>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    Corrija los siguientes errores
                    {{--<ul>--}}
                    {{--@foreach($errors->all() as $error)--}}
                    {{--<li>{{ $error }}</li>--}}
                    {{--@endforeach--}}
                    {{--</ul>--}}
                </div>
            @endif

            <form action="{{ route('users.update',$user) }}" method="POST">

                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="form-group">
                    <label for="name">Nombre:</label>
                    <input class="form-control"
                           aria-describedby="nameHelp"
                           type="text" name="name" placeholder="Juan P&eacute;rez" id="name" value="{{ old('name', $user->name ) }}"
                    >
                    @if ($errors->has('name'))
                        <small id="nameHelp" class="form-text text-danger">{{ $errors->first('name') }}</small>
                    @endif
                </div>

                <div class="form-group">
                    <label for="email">Correo electr&oacute;nico:</label>
                    <input class="form-control"
                           aria-describedby="emailHelp"
                           type="email" name="email" placeholder="jperez@example.com" id="email" value="{{ old('email', $user->email ) }}"
                    >
                    @if ($errors->has('email'))
                        <small id="emailHelp" class="form-text text-danger">{{ $errors->first('email') }}</small>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password">Clave:</label>
                    <input class="form-control"
                           aria-describedby="passwordlHelp"
                           type="password" name="password" placeholder="minimo 6 caracteres" id="password"
                    >
                    <small id="emailHelp" class="form-text text-muted">Dejarlo en blanco para mantener clave actual</small>
                    @if ($errors->has('password'))
                        <small id="passwordlHelp" class="form-text text-danger">{{ $errors->first('password') }}</small>
                    @endif
                </div>

                <div class="form-group">
                    <label for="bio">Biograf&iacute;a:</label>
                    <input class="form-control"
                           aria-describedby="bioHelp"
                           type="text" name="bio" placeholder="descr&iacute;bete (hasta 100 caracteres)" id="bio" value="{{ old('bio', $user->profile->bio ) }}"
                    >
                    @if ($errors->has('bio'))
                        <small id="bioHelp" class="form-text text-danger">{{ $errors->first('bio') }}</small>
                    @endif
                </div>

                <div class="form-group">
                    <label for="twitter">Twitter:</label>
                    <input class="form-control"
                           aria-describedby="twitterHelp"
                           type="text" name="twitter" placeholder="@your_twitter" id="name" value="{{ old('twitter', $user->profile->twitter ) }}"
                    >
                    @if ($errors->has('twitter'))
                        <small id="twitterHelp" class="form-text text-danger">{{ $errors->first('twitter') }}</small>
                    @endif
                </div>

                {{--<div class="d-flex justify-content-between align-items-end">--}}
                <button class="btn btn-primary" type="submit">Actualizar usuario</button>
                <a class="btn btn-link" href="{{ route('users.index') }}"><span class="oi oi-list"> Volver al listado</span></a>    {{--(with route)--}}
                <a class="btn btn-link" href="{{ route("users.show", $user) }}"><span class="oi oi-eye"> Volver al detalle</span></a>
                {{--</div>--}}

            </form>
        </div>
    </div>
@endsection

@section('sidebar')
    @parent
    <h3>Personalizada</h3>
@endsection